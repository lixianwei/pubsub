package util

func ContainString(s []string, e string) bool {
	for _, o := range s {
		if o == e {
			return true
		}
	}
	return false
}
