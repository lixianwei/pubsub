package server

import (
	"encoding/json"
	"gitlab.com/lixianwei/pubsub/proxy"
	"io"
	"net"
)

type TCPServer struct {
	host string
	errChan chan error
}

func NewTCPServer(host string) (tcp *TCPServer) {
	tcp = &TCPServer{host: host, errChan: make(chan error)}
	return
}

func (tcp *TCPServer) Start() {
	listen, err := net.Listen("tcp", tcp.host)
	if err != nil {
		tcp.errChan <- err
	}
	logger.Info().Msgf("TCP Listening %s", tcp.host)
	go func() {
		for {
			conn, err := listen.Accept()
			if err != nil {
				tcp.errChan <- err
				continue
			}
			go tcp.handleConn(conn)
		}
	}()
}

func (tcp *TCPServer) handleConn(conn net.Conn) {
	defer conn.Close()
	// 创建代理
	p := proxy.NewProxy()
	defer p.Close()

	go tcp.handleWrite(conn, p)
	tcp.handleRead(conn, p)
}

func (tcp *TCPServer) handleWrite(conn net.Conn, p *proxy.Proxy) {
	encoder := json.NewEncoder(conn)
	for msg := range p.Write {
		if err := encoder.Encode(msg); err != nil {
			tcp.errChan <- err
			// todo 是否应该break
			continue
		}
	}
}

func (tcp *TCPServer) handleRead(conn net.Conn, p *proxy.Proxy) {
	decoder := json.NewDecoder(conn)
	for {
		msg := proxy.Message{}
		if err := decoder.Decode(&msg); err != nil {
			switch err {
			case io.EOF:
				logger.Debug().Msg("client disconnect")
			case io.ErrUnexpectedEOF:
				logger.Debug().Msg("Client disconnected unexpectedly")
			default:
				tcp.errChan <- err
			}
			break
		}
		p.Read <- msg
	}
}