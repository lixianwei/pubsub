package proxy

type Message struct {
	Cmd    string   `json:"cmd"`
	Topics []string `json:"topics,omitempty"`
	Data   string   `json:"data,omitempty"`
}
