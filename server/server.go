package server

import (
	"errors"
	"fmt"
	"gitlab.com/lixianwei/pubsub/util"
	"net/url"
)

var (
	logger = util.GetConsoleLogger()
)

func Start(uris []string) {
	errChan := make(chan error)

	for _, uri := range uris {
		parsedUrl, err := url.Parse(uri)
		if err != nil {
			go func() {
				errChan <- errors.New(fmt.Sprintf("can not parse %s", uri))
			}()
		} else {
			switch parsedUrl.Scheme {
			case "ws":
				wsServer := NewWSServer(parsedUrl.Host)
				go wsServer.Start()
				go func() {
					for err := range wsServer.errChan {
						// log these errors and continue
						logger.Error().Err(err).Msg("StartWS")
					}
				}()
			case "tcp":
				tcpServer := NewTCPServer(parsedUrl.Host)
				go tcpServer.Start()
				go func() {
					for err := range tcpServer.errChan {
						// log these errors and continue
						logger.Error().Err(err).Msg("StartTCP")
					}
				}()
			default:
				go func() {
					errChan <- errors.New(fmt.Sprintf("%s not imp", parsedUrl.Scheme))
				}()
			}
		}
	}

	for err := range errChan {
		// log these errors and continue
		logger.Error().Err(err).Msg("When Server Start")
	}
}
