package server

import (
	"fmt"
	"github.com/gorilla/pat"
	"github.com/gorilla/websocket"
	"gitlab.com/lixianwei/pubsub/proxy"
	"net/http"
	"strings"
)

type WSServer struct {
	host string
	errChan chan error
}

func NewWSServer(host string) (ws *WSServer) {
	ws = &WSServer{host:host, errChan: make(chan error)}
	return
}

func (ws *WSServer) Start() {
	router := pat.New()
	router.Get("/upgrade", func(writer http.ResponseWriter, request *http.Request) {
		// 升级http
		upGrader := websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		}
		conn, err := upGrader.Upgrade(writer, request, nil)
		if err != nil {
			ws.errChan <- err
		}
		ws.handleConn(conn)
	})

	// start ws server
	logger.Info().Msg(fmt.Sprintf("WS Listening %s", ws.host))
	err := http.ListenAndServe(ws.host, router)
	if err != nil {
		ws.errChan <- err
	}
}

func (ws *WSServer)handleConn(conn *websocket.Conn) {
	defer conn.Close()

	// 创建代理
	p := proxy.NewProxy()
	defer p.Close()

	// write to conn
	go ws.handleWrite(conn, p)

	// read from conn
	// 不开启goroutine以阻塞此函数
	ws.handleRead(conn, p)
}

func (ws *WSServer)handleWrite(conn *websocket.Conn, p *proxy.Proxy) {
	for msg := range p.Write {
		if err := conn.WriteJSON(msg); err != nil {
			if err.Error() != "websocket: close sent" {
				ws.errChan <- err
			}
			// todo 是否应该break
			continue
		}
	}
}

func (ws *WSServer)handleRead(conn *websocket.Conn, p *proxy.Proxy) {
	for {
		msg := proxy.Message{}
		if err := conn.ReadJSON(&msg); err != nil {
			if !strings.Contains(err.Error(), "websocket: close 1001") && !strings.Contains(err.Error(), "websocket: close 1006") {
				ws.errChan <- err
			}
			// todo 是否应该break
			break
		}
		p.Read <- msg
	}
}
