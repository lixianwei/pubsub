package proxy

import (
	"gitlab.com/lixianwei/pubsub/util"
	"testing"
	"time"
)

func TestNewProxy(t *testing.T) {
	var (
		p1Id    = uint64(1)
		p2Id    = uint64(2)
		testMsg = "to p2"
	)
	p1 := NewProxy()
	p2 := NewProxy()
	if p1Id != p1.id || p2Id != p2.id {
		t.Error("proxy id error")
	}

	p1.Sub([]string{"hello"})
	p2.Sub([]string{"hello", "world"})
	go p1.Pub([]string{"hello", "exist"}, testMsg)
	select {
	case msg := <-p2.Write:
		t.Log(msg)
		if msg.Data != testMsg || !util.ContainString(msg.Topics, "hello") || util.ContainString(msg.Topics, "exist") {
			t.Error(msg)
		}
	case msg := <-p1.Write:
		t.Errorf("p1 should not receive msg %s", msg)
	case <-time.After(time.Second * 1):
		t.Error("spend much time")
	}
}
