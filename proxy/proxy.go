package proxy

import (
	"sync"
	"sync/atomic"
)

var (
	proxyId uint64 = 0
	proxies        = make(map[uint64]*Proxy)
	mutex          = &sync.RWMutex{}
)

type Proxy struct {
	id uint64

	Read   chan Message
	Write  chan Message
	Done   chan bool
	topics map[string]struct{}

	sync.RWMutex
}

func NewProxy() (p *Proxy) {
	p = &Proxy{
		id:     atomic.AddUint64(&proxyId, 1),
		Write:  make(chan Message),
		Read:   make(chan Message),
		Done:   make(chan bool),
		topics: make(map[string]struct{}),
	}
	mutex.Lock()
	defer mutex.Unlock()
	go p.handleMessage()
	proxies[p.id] = p
	return
}

func (p *Proxy) handleMessage() {
	for {
		select {
		case msg := <-p.Read:
			switch msg.Cmd {
			case Ping:
				p.Pong()
			case Sub:
				p.Sub(msg.Topics)
			case Unsub:
				p.Unsub(msg.Topics)
			case Pong:
			// todo nothing?
			case Pub:
				p.Pub(msg.Topics, msg.Data)
			}
		case <-p.Done:
			// 代理被终结
			return
		}
	}
}

func (p *Proxy) Ping() {
	p.Write <- Message{Cmd: Ping}
}

func (p *Proxy) Pong() {
	p.Write <- Message{Cmd: Pong}
}

func (p *Proxy) Sub(topics []string) {
	p.Lock()
	defer p.Unlock()
	for _, topic := range topics {
		p.topics[topic] = struct{}{}
	}
}

func (p *Proxy) Unsub(topics []string) {
	p.Lock()
	defer p.Unlock()
	for _, topic := range topics {
		delete(p.topics, topic)
	}
}

func (p *Proxy) Pub(topics []string, data string) {
	mutex.Lock()
	defer mutex.Unlock()
	for _, proxy := range proxies {
		if proxy.id == p.id {
			continue
		}
		p.RLock()
		var subTopics []string
		for _, topic := range topics {
			if _, ok := p.topics[topic]; ok {
				subTopics = append(subTopics, topic)
			}
		}
		p.RUnlock()
		if len(subTopics) > 0 {
			proxy.Write <- Message{Cmd: Pub, Topics: subTopics, Data: data}
		}
	}
}

func (p *Proxy) Close() {
	mutex.Lock()
	defer mutex.Unlock()
	delete(proxies, p.id)
	close(p.Read)
	close(p.Write)
}
