package proxy

const (
	Ping  = "ping"
	Pong  = "pong"
	Pub   = "pub"
	Sub   = "sub"
	Unsub = "unsub"
)

type Cmd interface {
	Ping()
	Pong()
	Sub(topics []string)
	Unsub(topics []string)
	Pub(topics []string, data string)
}
