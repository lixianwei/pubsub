## Pub/Sub RealTime App Core

### Support NetWork protocol
- WebSocket - `ws://localhost:5001`
- TCP - `tcp://localhost:5002`

### Message
All messages are JSON like this:
```go
package proxy

type Message struct {
	Cmd    string   `json:"cmd"`
	Topics []string `json:"topics,omitempty"`
	Data   string   `json:"data,omitempty"`
}
```
#### Message Commands
cmd     |   desc    |   topic |     data
---     |   ----    |   ----- |     ----
ping    |   ping    |   null  |     null
pong    |   pong    |   null  |     null
sub     | sub topic |   ["any"]    |     null
unsub   | unsub topic | ["any"]    |     null
pub     | pub topic |   ["any"]    | "string"

### How to use
```go
package main

import "gitlab.com/lixianwei/pubsub"

func main() {
	uris := []string{"ws://localhost:5001", "tcp://localhost:5002"}
	pubsub.Server(uris)
}
```
