package util

import (
	"github.com/rs/zerolog"
	"io"
	"os"
)

var (
	consoleLogger = ConsoleLogger()
)

func Logger(writer io.Writer) zerolog.Logger {
	logger := zerolog.New(writer)
	//logger.Level(level)
	return logger
}

func ConsoleLogger() zerolog.Logger {
	return Logger(os.Stdout)
}

func GetConsoleLogger() zerolog.Logger {
	return consoleLogger
}

func FileLogger(path string) zerolog.Logger {
	fd, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		// 日志不可用直接停止进程
		panic(err)
	}
	return Logger(fd)
}
