package pubsub

import "gitlab.com/lixianwei/pubsub/server"

func Server(uris []string) {
	server.Start(uris)
}
